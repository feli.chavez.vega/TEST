package com.felipe.heroes.services;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.felipe.heroes.models.usuarioModel;
import com.felipe.heroes.repositories.usuarioRepository;

@Service
public class usuarioService {
	
	@Autowired
	 usuarioRepository usuariorepository;
	
    public ArrayList<usuarioModel> obtenerUsuarios(){
    	return (ArrayList<usuarioModel>) usuariorepository.findAll();
    }
    
    
    public usuarioModel guardarUsuario(usuarioModel usuario) {
    	
    	try 
    	{
    		return usuariorepository.save(usuario);
    	}
    	catch (Exception e) 
    	{
			
    		usuarioModel usuarioModel = new usuarioModel();
    		usuarioModel.setComment("no se pudo agregar usuario, validar email y rut respectivamente");
    		return usuarioModel;
		}
		
    	
    }
    
    public Optional<usuarioModel> obtenerPorId(Long id){
    	
    	try
    	{
    		return usuariorepository.findById(id);
    	}
    	catch (Exception e)
    	{
    		return null;
    	}
    	
    }
    
    public ArrayList<usuarioModel> obtenerPrioridad(Integer prioridad){
    	
    	try
    	{
    		return   usuariorepository.findByPrioridad(prioridad);
    	}
    	catch (Exception e)
    	{
    		return null;
    	}

    }
    
    public boolean eliminarUsuario(Long id) {
    	
    	try 
    	{
    		usuariorepository.deleteById(id);
    		return true;
    	}
    	catch (Exception err) 
    	{
    		return false;
    	}
    }
    
}
