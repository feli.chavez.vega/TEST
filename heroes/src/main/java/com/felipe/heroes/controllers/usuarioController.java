package com.felipe.heroes.controllers;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.felipe.heroes.models.usuarioModel;
import com.felipe.heroes.services.usuarioService;

@RestController
@RequestMapping("/usuario")
public class usuarioController {
	@Autowired
	usuarioService usuarioService;
	
	
	@GetMapping()
	public ArrayList<usuarioModel> obtenerusuario(){
		return usuarioService.obtenerUsuarios();
	}
	
	@PostMapping()
	public usuarioModel guardarUsuario(@RequestBody usuarioModel usuario) {
		return this.usuarioService.guardarUsuario(usuario);	
	}

	 
	@GetMapping(path = "/{id}")
	public Optional<usuarioModel> obtenerUsuarioPorId(@PathVariable("id") Long id){
		return this.usuarioService.obtenerPorId(id);
	}
	
	@GetMapping("/query")
	public ArrayList<usuarioModel> obtenerUsuarioPorPrioridad(@RequestParam("prioridad") Integer prioridad){
		return this.usuarioService.obtenerPrioridad(prioridad);
	}
	
	@DeleteMapping (path = "/{id}")
	public String eliminarPorId(@PathVariable("id") Long id) {
		
		boolean ok = this.usuarioService.eliminarUsuario(id);
		if(ok)
		{
			return "Se elimino el usuario ID : " + id;
		}
		else
		{
			return "No se elimino el usuario ID : " + id;
		}
	}
	
}
